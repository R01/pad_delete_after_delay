FROM perl:5-buster
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y install build-essential libssl-dev libio-socket-ssl-perl
RUN cpan Carton
RUN mkdir -p /opt/delete_after_delay
COPY ./delete_after_delay.pl ./delete_after_delay ./cpanfile.snapshot ./cpanfile /opt/delete_after_delay/
WORKDIR /opt/delete_after_delay
RUN carton install
COPY ./docker/init /
ENV READ_FILE_API_KEY_RETRIES 10
ENV READ_FILE_API_KEY_DELAY_BETWEEN_RETRIES_SECONDS 30
ENV QUERY_API_RETRIES 10
ENV QUERY_API_DELAY_BETWEEN_RETRIES_SECONDS 30
ENTRYPOINT ["/init"]
